import json, math, copy, random


# Skapar en funktion som beräknar hur mycket av varje resurs som kommer dras under en period
# In-argumenten bestämmer vad du vill att 
def calculate_consumed(avg_water, avg_food, avg_electric):
    keys = ["water", "food", "electric"]
    consumed = {
        "water": -avg_water,
        "food": -avg_food,
        "electric": -avg_electric
    }
    for key in keys:
        consumed[key] = round(consumed[key]*random.normalvariate(1,0.1))

    return consumed
