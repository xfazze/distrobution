import json
def calculate_storage(consumed, produce):
    keys = ["water", "food", "electric"]
    storage = json.load(open('/home/xfazze/distrobution/distrobution_system/system/storage.json', 'r'))
        
    for key in keys:
        storage[key] = storage[key]+produce[key]+consumed[key]
        if storage[key] < 0.0:
            print("storage.py not enough ", key, "in storage by ", storage[key])
            storage[key] = 0
    
    with open('/home/xfazze/distrobution/distrobution_system/system/storage.json', 'w') as file:
        json.dump(storage, file, indent=4)
    return storage