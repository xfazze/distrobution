import json, copy  # importera json och copy bibliotek
from mass_consume import calculate_consumed #importera funktionen calculate_consumed från filen mass_consume
from produce import calculate_produced #importera funktionen calculacalculate_producedte_consumed från filen produce
from storage import calculate_storage #importera funktionen calculate_storage från filen storage


# Skapar en funktion som kör de olika funktionerna i rätt ordning för att representera en period
# In-argumentationerna går direkt till calculate_consumed
def run_period(input_water_consumption, input_food_consumption, input_electricity_consumption):
    consumed = calculate_consumed(input_water_consumption, input_food_consumption, input_electricity_consumption) # 

    history = json.load(open('/home/xfazze/distrobution/distrobution_system/system/history.json', 'r'))

    history[0] = copy.deepcopy(history[1])
    history[1] = copy.deepcopy(history[2])
    history[2] = copy.deepcopy(history[3])
    history[3] = consumed

    with open('/home/xfazze/distrobution/distrobution_system/system/history.json', 'w') as file:
        json.dump(history, file, indent = 4)

    produce = calculate_produced()
    storage = calculate_storage(consumed, produce)
    return [produce, consumed, storage]


for i in range(1000):
    output = run_period(100,1000,100)

